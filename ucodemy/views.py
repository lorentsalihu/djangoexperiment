from news.views import HomePage as NewsHomePage
from cslist.views import cslist_context_data

class HomePage(NewsHomePage):
	 template = "home.html"

	 def get_context_data(self, **kwargs):
	 	context = super(HomePage, self).get_context_data(**kwargs)
	 	cslist_context_data(context, self.request)
	 	return context
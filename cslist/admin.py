from django.contrib import admin
from . import models

# Register your models here.


class PlatformInline(admin.TabularInline):
	model = models.Platform
	extra = 1

class PriceTierInline(admin.TabularInline):
	model = models.PriceTier
	extra = 1

class ScreenShotInline(admin.TabularInline):
	model = models.ScreenShot
	extra = 1

# class LanguageInline(admin.TabularInline):
# 	model = models.Language
# 	extra = 1

@admin.register(models.CSResource)
class CsResourceAdmin(admin.ModelAdmin):
	inlines = [
		PlatformInline,
		PriceTierInline,
		ScreenShotInline,
		#LanguageInline,		
	]
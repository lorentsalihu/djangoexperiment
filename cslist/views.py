from django.shortcuts import render
from .models import CSResource

# Create your views here.
def cslist_context_data(context, request):
  context['table'] = CSResource.objects.all()